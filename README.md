# Docker Images

---App 1 browser output: Hello I am App 1:

	$ docker pull masumfaisal/docker_images:bs23_app1
	$ docker run -d --name app1 -p 9001:80 masumfaisal/docker_images:bs23_app1
	$ curl http://localhost:9001

---App 2 browser output: Hello I am App 2:

	$ docker pull masumfaisal/docker_images:bs23_app2
	$ docker run -d --name app2 -p 9002:80 masumfaisal/docker_images:bs23_app2
	$ curl http://localhost:9002

# Directory Structure
---app1

	---src
	
	---build
	
	---deploy
	
	---Jenkinsfile
	
	---readme.md

---app2
	
	---src
	
	---build
	
	---deploy
	
	---Jenkinsfile
	
	---readme.md

---nginx
	
	---deploy
	
	---redme.md

---infra
	
	---config
	
	---readme,md

---readme.md

